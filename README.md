# Unofficial “odd-jobbed rankings”

This “rankings” is almost certainly riddled with errors, inaccuracies, and
missing information, and should be treated as such. This is just for informal
use, so please don’t take it too seriously. The levels of the characters listed
here are fetched directly from [the official MapleLegends
rankings](https://maplelegends.com/ranking/all) via [a shitty Python
script](https://codeberg.org/oddjobs/odd-jobbed_rankings/src/branch/master/update.py).

Because the impetus of this “rankings” was the formation of groups for
bossing/PQing, only off-island characters are represented here. In addition, to
make the “rankings” actually maintainable, characters who have not yet achieved
level 45 are not represented here either. Supposedly, only active players
(“player” should be distinguished from “player character”) are represented
here.

“IGN” stands for “in-game name”. The “name” entries are mostly for discerning
when two or more characters are controlled by the same player. The names, I’ve
done on a best-effort basis, and some of them are just Discord identifiers
(which, it should be noted, can be changed at more or less any time, for any
reason).

Unknown or uncertain information is denoted by a question mark (“?”).

\*Not a member of <b>Suboptimal</b>.

| IGN        | name         | level | job(s)                 | guild         |
| :--------- | :----------- | ----: | :--------------------- | ------------- |
| Otios | ? | 135 | STRginner | Flow |
| LawdHeComin | ? | 129 | F/P archgish | Oddjobs |
| rusa | deer | 123 | darksterity knight | Oddjobs |
| cervid | deer | 122 | STR bishop | Oddjobs |
| capreolina | deer | 121 | woodsmaster | Oddjobs |
| Tacgnol | Kelsey | 121 | F/P archgishlet | Oddjobs |
| LoneW0lf1600 | LoneWolf1600 | 110 | STRginner | Flow |
| cervine | deer | 107 | I/L magelet | Oddjobs |
| Taima | Kelsey | 106 | STRginner | Oddjobs |
| Gumby | Jonathan | 105 | STRginner | Flow |
| OmokTeacher | Noam | 101 | STRginner | Flow |
| ducklings | joyce | 97 | STRginner | DuckNation\* |
| Outside | Tab | 95 | STRginner | Flow |
| Permanovice | Hanger | 92 | STRginner | GangGang\* |
| hydropotina | deer | 91 | swashbuckler | Oddjobs |
| MeikoHonma | Meiko | 87 | STRginner | Renaissance\* |
| Cowbelle | Belle | 86 | STRginner | Homies\* |
| Boymoder | Kelsey | 86 | STRmit | Oddjobs |
| gogigagagigo | boop | 86 | permarcher | Oddjobs |
| Cortical | Andrew Tran | 85 | STRginner | Flow |
| xX17Xx | mae | 85 | permarogue | Oddjobs |
| GishGallop | Andrew Tran | 85 | I/L gish | Oddjobs |
| alces | deer | 85 | daggermit | Oddjobs |
| Ismezin | Zin | 83 | STRginner | Flow |
| rangifer | deer | 76 | pugilist (marauder) | Oddjobs |
| Daddyo | Hunter | 76 | STRginner | \[none\]\* |
| Rort | Lin | 75 | STRginner | Flow |
| Phoneme | Andrew Tran | 74 | permamagician | Oddjobs |
| Celim | Marcelo | 73 | STRginner | Flow |
| drainer | mae | 72 | STRginner | Flow |
| justbegin | ? | 65 | STRginner | GangGang\* |
| Furbs | ? | 63 | STRginner | WindowsXP\* |
| BeginnersEnd | Ben | 63 | STRginner | Flow |
| Hanyou | Kelsey | 58 | DEX page | Oddjobs |
| Medulla | Andrew Tran | 57 | dagger fighter | Oddjobs |
| JanitorPedro | Pedro\_ | 56 | STRginner | Flow |
| Slimusaurus | Noam | 55 | besinner | \[none\]\* |
| Copo | Marcelo | 54 | permapirate | Oddjobs |
| L0neW0lf16OO | LoneWolf1600 | 54 | besinner | Oddjobs |
| hashishi | deer | 53 | besinner | Oddjobs |
| HPdit | RyeBread | 53 | blood bandit | Oddjobs |
| Numidium | Kelsey | 52 | STR cleric | Oddjobs |
| Swathelby | ? | 51 | STRginner | Flow |
| Gambolpuddy | Kelsey | 50 | DEXginner | Oddjobs |
| sorts | deer | 50 | DEX brawler (LPQ mule) | Pals\* |
| NightDood | ? | 50 | stab sin | \[none\]\* |
| Sommer | Marcelo | 49 | swashbuckler, punch slinger | Oddjobs |
| doiob | doiob | 48 | STR cleric | Oddjobs |
| BowerStrike | Andrew Tran | 48 | bow-whacker | Oddjobs |
| FairRow | FairRow | 47 | STRginner | Oddjobs |
| ScrubDaddy | Scrub | 46 | STRginner | Oddjobs |
| HPdagger | Charles | 45 | blood dagger fighter | Oddjobs |
| Monc | Monc | 45 | permamagician | Oddjobs |
